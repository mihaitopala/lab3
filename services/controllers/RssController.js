import DB from '../Database';

const getAllRss = () => {
  return new Promise((resolve, reject) => {
    DB.transaction(txn => {
      txn.executeSql(`SELECT * from links`, [], (tx, results) => {
        const len = results.rows.length;
        const data = [];
        
        if (len === 0) reject(data)

          for (let i = 0; i < len; ++i) {
            data.push(results.rows.item(i));
          }
          
          const final = data.map(item => {
            const content = JSON.parse(item.content);
            return {
              id: item.id,
              title: item.title,
              description: content.description[0],
              items: content.item
            }
          });

          resolve(final);
      });
    });
  });
};

const getRssContentById = () => {
  return new Promise((resolve, reject) => {
    DB.transaction(txn => {
      txn.executeSql('SELECT * FROM links where id=?', [id], (tx, results) => {
        if (results.rowsAffected > 0) {
          resolve({success: true});
        } else {
          reject(new Error('Failed to insert rss!'));
        }
      });
    });
  });
};

const saveRssContent = data => {
  console.log('data to insert', data)
  return new Promise((resolve, reject) => {
    DB.transaction(txn => {
      txn.executeSql(
        'INSERT into links (title, content) VALUES (?, ?)',
        [data.title[0], data.content],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            resolve({success: true});
          } else {
            reject(new Error('Failed to insert rss!'));
          }
        },
      );
    });
  });
};

const deleteRss = id => {
  return new Promise((resolve, reject) => {
    DB.transaction(txn => {
      txn.executeSql('DELETE FROM links where id=?', [id], (tx, results) => {
        if (results.rowsAffected > 0) {
          resolve({success: true});
        } else {
          reject(new Error('Failed to delete rss'));
        }
      });
    });
  });
};

module.exports = {
  getAllRss,
  getRssContentById,
  saveRssContent,
  deleteRss,
};
