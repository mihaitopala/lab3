import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

const database_name = 'rss.sqlite';

const onSuccess = () => {
    console.log('DB connected!')
}

const onError = (error) => {
    console.log('error', error)
}

const db = SQLite.openDatabase({ name: database_name, createFromLocation: 1, }, onSuccess, onError);

export default db;