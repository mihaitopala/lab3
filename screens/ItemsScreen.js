import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Linking,
} from 'react-native';
import moment from 'moment';

import Title from "../components/header/Title";

class RssItems extends Component {
    static navigationOptions = () => {
        return {
          headerTitle: () => <Title title="RSS items" />,
        };
      };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const {navigation} = this.props;

    const items = navigation.getParam('items');

    if (items && items.length) {
      this.setState({data: items});
    }
  }

  onPressButton = url => {
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  };

  render() {
    const {data} = this.state;

    return (
      <View style={[styles.container]}>
        <FlatList
          style={styles.list}
          data={data}
          renderItem={({item, index}) => (

            <TouchableOpacity onPress={() => this.onPressButton(item.link[0])}>
              <View style={styles.subContainer}>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>
                    <Text style={{fontWeight: 'bold'}}>Title:</Text>{' '}
                    {item.title[0]}
                  </Text>
                </View>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>
                    <Text style={{fontWeight: 'bold'}}>Description:</Text>{' '}
                    {item.description[0]}
                  </Text>
                </View>
                <View style={styles.listItemCont}>
                  <Text style={styles.listItem}>
                    <Text style={{fontWeight: 'bold'}}>Created at:</Text>{' '}
                    {moment(item.pubDate[0]).format('YYYY-MM-DD')}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default RssItems;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    paddingTop: 20,
  },
  subContainer: {
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 10,
    padding: 10,
  },
  list: {
    width: '100%',
  },
  listItem: {
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 18,
  },
  listItemCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
