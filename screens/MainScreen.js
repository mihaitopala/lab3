import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Title from '../components/header/Title';
import AddFeedModal from '../components/modals/AddRssModal';
import RssList from '../components/list/RssList';
import {getAllRss} from '../services/controllers/RssController';

const links = [
  {
    title: 'www.espn.com - MLB',
    link: 'https://www.espn.com/espn/rss/mlb/news'
  },
  {
    title: 'BBC News - World',
    link: 'http://feeds.bbci.co.uk/news/world/rss.xml',
  },
  {
    title: 'E-International Relations',
    link: 'https://www.e-ir.info/category/blogs/feed/',
  },{
    title: 'Al Jazeera RSS Feed',
    link: 'https://www.aljazeera.com/xml/rss/all.xml'
  }
];

class MainScreen extends Component {
  static navigationOptions = () => {
    return {
      headerTitle: () => <Title title="Rss links list" />,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      showAddModal: false,
      rss_data: [],
      loading: false,
    };
  }

  componentDidMount() {
    this.getExistingRss();
  }

  getExistingRss = async () => {
    try {
      this.setState({loading: true});
      const result = await getAllRss();

      if (result) {
        this.setState({
          rss_data: result,
          loading: false,
        });
      }
    } catch (error) {
      this.setState({ loading: false })
    }
  };

  openModal = () => {
    this.setState({showAddModal: true});
  };

  closeModal = () => {
    this.setState({showAddModal: false});
  };

  render() {
    const {showAddModal, loading, rss_data} = this.state;
    const titles = rss_data.map(item => item.title);

    const {navigation} = this.props;

    if (loading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.sectionContainer}>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.buttonContainer}>
              <Icon.Button
                style={styles.addButton}
                name="plus"
                onPress={this.openModal}>
                Add
              </Icon.Button>
            </View>
            <RssList navigation={navigation} list={rss_data} />
          </ScrollView>
        </SafeAreaView>
        <AddFeedModal
          visible={showAddModal}
          values={links}
          existigItems={titles}
          handleClose={this.closeModal}
          loadData={this.getExistingRss}
        />
      </>
    );
  }
}

export default MainScreen;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  scrollView: {
    marginHorizontal: 0,
  },
  sectionContainer: {
    marginTop: 15,
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'flex-end', 
    marginRight: 10
  },
  addButton: {
    borderRadius: 50,
    width: 100,
    height: 40,
  },
});
