import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import MainScreen from './screens/MainScreen';
import RssItems from './screens/ItemsScreen';

const MainNavigator = createStackNavigator(
  {
    Main: { screen: MainScreen },
    RssItem: { screen: RssItems }
  },
  {
    headerLayoutPreset: 'center'
  }
);

const App = createAppContainer(MainNavigator);

export default App;
