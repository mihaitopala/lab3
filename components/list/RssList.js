import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList, Alert} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import {deleteRss} from '../../services/controllers/RssController';

class RssList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.list || [],
    };
  }

  handleDelete = async id => {
    try {
      this.setState({loading: true});
      const response = await deleteRss(id);
      if (response) {
        const newArr = this.state.data.filter(item => item.id !== id);
        this.setState({
          data: newArr,
          loading: false,
        });
      }
    } catch (error) {
      this.setState({loading: false});
      Alert.alert('Error', error.message, [
        {
          text: 'Ok',
        },
      ]);
    }
  };

  render() {
    const {data} = this.state;
    const {navigation} = this.props;

    return (
      <View style={[styles.container]}>
        <FlatList
          style={styles.list}
          data={data}
          renderItem={({item, index}) => (
            <View style={styles.subContainer}>
              <View style={styles.listItemCont}>
                <Text style={styles.listItem}>
                  <Text style={{fontWeight: 'bold'}}>Title:</Text> {item.title}
                </Text>
              </View>
              <View style={styles.listItemCont}>
                <Text style={styles.listItem}>
                  <Text style={{fontWeight: 'bold'}}>Description:</Text>{' '}
                  {item.description}
                </Text>
              </View>
              <View style={styles.blockButtons}>
                <Icon.Button
                  style={styles.deleteBtn}
                  name="trash"
                  onPress={() => this.handleDelete(item.id)}>
                  Delete
                </Icon.Button>
                <Icon.Button
                  style={styles.viewBtn}
                  name="eye"
                  onPress={() => navigation.push('RssItem', {items: item.items})}>
                  View data
                </Icon.Button>
              </View>
            </View>
          )}
        />
      </View>
    );
  }
}

export default RssList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    paddingTop: 20,
  },
  subContainer: {
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 10,
    padding: 10,
  },
  list: {
    width: '100%',
  },
  listItem: {
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 18,
  },
  listItemCont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  deleteBtn: {
    backgroundColor: '#d11a2a',
    textAlign: 'center',
    fontSize: 16,
  },
  blockButtons: {
    margin: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
