import React, {Component} from 'react';
import {parseString} from 'react-native-xml2js';
import {View, Text, StyleSheet, Picker, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';

import {saveRssContent} from '../../services/controllers/RssController';

class AddRssModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedValue: props.values.length ? props.values[0].link : null,
    };
  }

  handleDownload = () => {
    const {selectedValue} = this.state;

    if (selectedValue) {
      fetch(`${selectedValue}`, {
        method: 'GET',
      })
        .then(response => response.text())
        .then(xml => {
          parseString(xml, async (err, result) => {
            const {title, ...rest} = result.rss.channel[0];
            const data = await saveRssContent({
              title,
              content: JSON.stringify(rest),
            });

            if (data) {
              this.props.loadData();
              this.props.handleClose();
            }
          });
        })
        .catch(error => {
          Alert.alert('Error', 'Failed to load resource', [
            {
              text: 'Ok',
            },
          ]);
        });
    }
  };

  render() {
    const {values, visible, handleClose, existigItems} = this.props;
    const options = values.map(item => (
      <Picker.Item label={`${item.title}`} value={item.link} />
    ));

    return (
      <View style={styles.sectionContainer}>
        <Modal 
          isVisible={visible}
          animationIn={'slideInLeft'}
          animationOut={'slideOutRight'}
          onBackdropPress={handleClose}
        >
          <View style={styles.modalContent}>
            <View>
              <Text style={styles.title}>Select RSS to download </Text>
              <View>
                <Picker
                  selectedValue={this.state.selectedValue}
                  style={styles.select}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({selectedValue: itemValue})
                  }>
                  {options}
                </Picker>
              </View>
            </View>
            <View style={styles.bottomButtons}>
              <Icon.Button
                style={styles.downloadButton}
                name="download"
                onPress={this.handleDownload}>
                Download feed
              </Icon.Button>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default AddRssModal;

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    position: 'relative'
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  bottomButtons: {
    flexDirection: 'column',
    backgroundColor: '#ccc'
  },
  downloadButton: {
    backgroundColor: '#3b5998',
    height: 45,
  },
  select: {
    width: 250,
    height: 50,
    borderWidth: 1,
  },
});
